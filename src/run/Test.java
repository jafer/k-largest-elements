package run;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

import selectionAlgorithm.HeapTree;
import selectionAlgorithm.QuickSelect;
import utils.Results;

public class Test {

	public static ArrayList<Integer> readIntance(File file) throws IOException {
		Scanner scan = new Scanner(file);
		ArrayList<Integer> array = new ArrayList<Integer>();
		
		while(scan.hasNext()) {
			array.add(scan.nextInt());
		}

		return array;
	}

	public static void main(String[] args) throws IOException {
		double time;
		Results metrics = new Results();
		int[] size = { 100, 1000, 10000 };
		double[] rate = {0.25, 0.50, 0.75};
		String[] orderString = { "DESC", "NO_ORDER", "ASC" };
		int numberOfEvaluations = 100;
		ArrayList<Double> results;
		DecimalFormat fmt = new DecimalFormat("####.##");
		for (int i = 0; i < size.length; i++) {
			for (int j = 0; j < rate.length; j++) {
				for (int j2 = 0; j2 < orderString.length; j2++) {
					ArrayList<Integer> elements = readIntance(new File("in/I_"
							+ size[i] + "_" + orderString[j2] + ".dat"));
					
					QuickSelect<Integer> algorithm = new
					QuickSelect<Integer>(elements);
					results = new ArrayList<Double>(numberOfEvaluations);
					for (int k = 0; k < numberOfEvaluations; k++) {
						
						// runTime
						
						time = System.currentTimeMillis();

						//HeapTree<Integer> algorithm = new HeapTree<>(elements);
						elements = algorithm.getLargestElements((int) (rate[j] * size[i]));
						time = System.currentTimeMillis() - time;
						//
						results.add(time);
						
					}
					double average = metrics.getAverage(results);
					double devian = metrics.getStandardDeviation(average, results);
					
				System.out.print(fmt.format(average)+"+/-"+fmt.format(devian)+" ");
					
				}
				System.out.println();
			}
		}

	}
}
